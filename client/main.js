RaceLogicRefreshInterval = null;
timeOffset = 0;


subsRacers = Meteor.subscribe('races');
Meteor.subscribe('raceConfigs');
Meteor.subscribe('racers');
Meteor.subscribe('config');
Meteor.subscribe('console');
Meteor.subscribe('raceSessions');
Meteor.subscribe('times');
accountsUIBootstrap3.setLanguage('ru');

tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,',
    header = '<?xml version="1.0" encoding="UTF-8" ?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40"><Styles><Style ss:ID="bold" ss:Name="bold"><Font ss:Bold="1"/></Style></Styles>',
    base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))); }
  return function(table, name) {
    var tpl = '<Worksheet ss:Name="Стр 1"><Table>';
    var tableEl = document.getElementById(table);

    for(var i=0,c=tableEl.childNodes,l=c.length; i<l; i++){
        if(c[i].nodeType === 1){
            for(var ii=0,cc=c[i].childNodes,ll=cc.length; ii<ll; ii++){
                if(cc[ii].nodeType === 1){
                    tpl += '<Row>';
                    for(var iii=0,ccc=cc[ii].childNodes,lll=ccc.length; iii<lll; iii++){
                        if(ccc[iii].nodeType === 1 && ccc[iii].className !== 'action'){
                            if(c[i].tagName === "THEAD"){
                                tpl += '<Cell ss:Index="1" ss:StyleID="bold"><Data ss:Type="String">' + ccc[iii].innerHTML.replace(/\s{2,}/g, '') + '</Data></Cell>';
                            }
                            else{
                                tpl += '<Cell ss:Index="1"><Data ss:Type="String">' + ccc[iii].innerHTML.replace(/\s{2,}/g, '') + '</Data></Cell>';
                            }
                        }
                    }
                    tpl += '</Row>';
                }
            }
        }
    }
    tpl+= '</Table></Worksheet></Workbook>'

    var link = document.createElement("a");
    link.setAttribute("href", uri + base64(header + tpl));
    link.setAttribute("download", "export.xls");
    link.click();
  }
})()
