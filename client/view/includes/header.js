Template.header.helpers({
    serialPortStatus: function () {
        if(Config.findOne()){
            return Config.findOne().serialPortStatus;
        }
        else{
            return 0;
        }
    },
    serialPortName: function(){
        if(Config.findOne()){
            return Config.findOne().selectedSerialPort;
        }
        else{
            return '';
        }
    },
    consoleData:function(){
        return Console.find({});
    }
});


Template.header.events({
    'click #spStatus': function(e,t){
        e.preventDefault();
        Meteor.call('serialPortConnect');
    },
    'click #consoleBtn': function(e,t){
        e.preventDefault();
        console.log('console')
    }
});
