window.A = new ReactiveVar(moment().valueOf());

//console.log('lol', moment().valueOf())

Template.race.helpers({
    activeRace: function () {
        return Races.find({active: true}, {fields: {racers: 1}}).fetch().length>0 ? true: false;
    },
    racers: function () {
        /*if(Races.find({active: true}, {fields: {racers: 1}}).fetch()[0]){
            return Races.find({active: true}, {fields: {racers: 1}}).fetch()[0].racers
        }
        else{
            return [];
        }*/
        //return Racers.find();
        return _.map(Racers.find({}, {sort: {bestTime: 1}}).fetch(), function(value, index){
            return _.extend(value, {index: index+1});
        });
    },
    curRace: function() {
        return Races.find({active: true}, {fields: {raceName: 1, createdAt: 1}}).fetch()[0];
    },
    counter: function(options) {
        //console.log('counter time', this, options.hash)
        var curRace = Races.find({ "active": true }).fetch()[0];
        switch(options.hash.action){
            case "t":
                if(this.racerTimes[curRace.raceLaps * options.hash.var-1]){
                    return moment(this.racerTimes[curRace.raceLaps * options.hash.var - 1] -
                         this.racerTimes[curRace.raceLaps * options.hash.var - curRace.raceLaps]).format('mm:ss,SSS');
                }
                else if(this.racerTmpTimes[curRace.raceLaps * options.hash.var - curRace.raceLaps]){
                    return moment(window.A.get() -
                        (this.racerTmpTimes[curRace.raceLaps * options.hash.var - curRace.raceLaps] - timeOffset)).format('mm:ss,SSS');
                }
                else{
                    return '--//--';
                }
            case '-':
                if(this.racerTimes[options.hash.var1]){
                    return moment(this.racerTimes[options.hash.var1] - this.racerTimes[options.hash.var2]).format('mm:ss,SSS');
                }
                else if(this.racerTmpTimes[options.hash.var2]){
                    return moment(window.A.get() - (this.racerTmpTimes[options.hash.var2] - timeOffset)).format('mm:ss,SSS');
                }
                else{
                    return '--//--';
                }

            case 'bestTime':
                if(this.bestTime && this.bestTime !== 9999999999){
                    return moment(this.bestTime).format('mm:ss,SSS');
                }
                else{
                    return '--//--';
                }
        }

    }
});

Template.race.events({
    'click #addRacerBtn': function(e,t){
        e.preventDefault();
        var racerName = t.find('#racerName').value,
            racerSurname = t.find('#racerSurname').value,
            racerNumber = t.find('#racerNumber').value,
            racerCar = t.find('#racerCar').value,
            racerCarType = t.find('#racerCarType').value;

        console.log('arr new racer', Races.findOne())
        var races = Races.find({active: true}).fetch()[0];
        if(races){
            Racers.insert({ racerName: racerName, racerSurname: racerSurname,
                racerNumber: racerNumber, racerCar: racerCar, racerCarType: racerCarType, racerTimes: [], racerTmpTimes: []});

            $('#addRacerModal').modal('hide')
        }
        else{
            console.error('Нет активных заездов!! сделать всплывающее окно!!')
        }

    },
    'click .startBtn': function(e,t){
        e.preventDefault();


        var racerId = this._id;
        var curRace = Races.find({active: true}).fetch()[0];

        Meteor.call('raceLogicAddRacer', racerId);
        if(!RaceLogicRefreshInterval){
            TimeSync.resync();
            timeOffset = TimeSync.serverOffset();

            RaceLogicRefreshInterval = setInterval(function(){

                Meteor.call('raceLogicRacersCount', function(err, data){
                    if(data === 0){
                        clearInterval(RaceLogicRefreshInterval);
                        RaceLogicRefreshInterval = null;
                    }
                    else{
                        window.A.set(moment().valueOf());
                    }
                });


            }, 55);
        }
        //console.log('racersInRace', Meteor.call('raceLogicRacersCount'))
    },
    'click .removeBtn': function(e,t){
        e.preventDefault();
        Racers.remove(this._id);
    },
    'click #clearBtn': function(e,t){
        Meteor.call('raceLogicClearRacers');
    },
    'click #clearAllTimesBtn': function(e,t){
        Meteor.call('raceLogicClearAllTimes');
    },
    'click #generateDetectLine1': function(e,t){
        var time = new Date();
        Meteor.call('raceLogicIncoming', "1", "L", time.getTime());

    },
    'click #generateDetectLine2': function(e,t){
        var time = new Date();
        Meteor.call('raceLogicIncoming', "2", "L", time.getTime());

    },
    'click #tstTime': function(e,t){
        var time = new Date();
        console.log(timeOffset);

    },
    'click #exportXML': function(e,t){
        e.preventDefault();
        tableToExcel('testTable', 'W3C Example Table');
        //window.open('data:application/vnd.ms-excel,', 'download');
    }
});
