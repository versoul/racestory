Template.raceCreate.helpers({
    races: function () {
        return Races.find({});
    },
    raceConfigs: function () {
        return raceConfs;
    }
});


Template.raceCreate.events({
    'click #saveBtn': function(e,t){
        e.preventDefault();
        var name = t.find('#name').value,
            conf = parseInt(t.find('#conf').value);

        console.log('savee', name, conf)
        Races.insert({ name: name, conf: conf,
            createdAt: new Date(), sessions: [], active:false,
            racers:[]});

        Router.go('race_list');
    }
});
Template.raceCreate.events({
    'click #saveStartBtn': function(e,t){
        e.preventDefault();
        var raceName = t.find('#raceName').value,
            raceConfig = parseInt(t.find('#raceConfig').value),
            raceLaps = t.find('#raceLaps').value,
            raceStart = t.find('#raceStart').value,
            raceFinish = t.find('#raceFinish').value;

        console.log('savee', raceName, raceConfig)
        Races.insert({ raceName: raceName, raceLaps: raceLaps,
             raceStart:raceStart, raceFinish:raceFinish, createdAt: new Date(),
              racers: [], racersInRace: [], racerTimes: {}, active: true });

        Router.go('race');
    }
});
