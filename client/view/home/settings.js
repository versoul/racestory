Template.settings.helpers({
    serialPortsList: function () {
        var conf = Config.findOne();
        if(conf){
            var modPorts = _.map(conf.serialPortsList, function(key){
                if(key.comName === conf.selectedSerialPort){
                    key.selected = true;
                }
                return key;
            })
            return modPorts;
        }
        else{
            return [];
        }
    }
});


Template.settings.events({
    'click #refreshBtn': function(e,t){
        Meteor.call('serialPortsRefresh');
    },
    'click #saveBtn': function(e,t){
        e.preventDefault();
        var serialPort = t.find('#serialPort').value;
        Meteor.call('serialPortSelect', serialPort);
    }
});
