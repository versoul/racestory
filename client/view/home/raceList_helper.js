Template.raceList.helpers({
    races: function () {
        return Races.find({});
    }
});

Template.raceList.events({
    'click .goBtn': function(e,t){
        e.preventDefault();
        Router.go('/race/'+this._id);
    },
    'click .removeBtn': function(e,t){
        e.preventDefault();
        Races.remove(this._id);
    }
});
