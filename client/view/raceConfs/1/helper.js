

Template.t_1.rendered = function() {
    window.A = new ReactiveVar(moment().valueOf());
    Template.t_1.maxSession = RaceSessions.find({race: Router.current().params._id}).count();
}
Template.t_1.helpers({
    race: function () {
        var curRace = Races.find(Router.current().params._id);
        return curRace.fetch()[0];
    },
    racers: function () {
        var filter = {};
        if(Session.get("carType")._id !== "all"){
            filter.racerCarType = Session.get( "carType")._id;
        }
        return _.map(Racers.find(filter).fetch(), function(value, index){
            return _.extend(value, {index: index+1});
        });
    },
    counter: function(options) {
        var curRace = Races.find().fetch()[0];//FIXME
        var times = Times.find({racerId: this._id, raceId: curRace._id,
             sessionId: Session.get( "raceSession")._id}).fetch();
         switch(options.hash.action){
             case "t":
                 if(times[2 * options.hash.var-1]){
                     return moment(times[2 * options.hash.var - 1].time -
                          times[2 * options.hash.var - 2].time).format('mm:ss,SSS');
                 }
                 else if(times[2 * options.hash.var - 2]){
                     return moment(window.A.get() -
                         (times[2 * options.hash.var - 2].tmpTime - timeOffset)).format('mm:ss,SSS');
                 }
                 else{
                     return '--//--';
                 }
             /*case '-':
                 if(this.racerTimes[options.hash.var1]){
                     return moment(this.racerTimes[options.hash.var1] - this.racerTimes[options.hash.var2]).format('mm:ss,SSS');
                 }
                 else if(this.racerTmpTimes[options.hash.var2]){
                     return moment(window.A.get() - (this.racerTmpTimes[options.hash.var2] - timeOffset)).format('mm:ss,SSS');
                 }
                 else{
                     return '--//--';
                 }

             case 'bestTime':
                 if(this.bestTime && this.bestTime !== 9999999999){
                     return moment(this.bestTime).format('mm:ss,SSS');
                 }
                 else{
                     return '--//--';
                 }*/
         }
        //return moment(times[1].time-times[0].time).format('mm:ss,SSS');
    },
    isActiveSession: function(){
        var conf = Config.findOne();
        if(Session.get("raceSession") && Session.get( "raceSession")._id == conf.raceSession){
            return true;
        }
        else{
            return false;
        }
    },
    sessions: function(){
        return RaceSessions.find({race: Router.current().params._id});
    },
    curSession:function(){
        var s = Session.get( "raceSession");
        if(s){
            return s.name;
        }
        else{
            return "Выберите сессию";
        }
    },
    carTypes: function(){
        return [
            {name: "Все", _id: "all"},
            {name: "Полный", _id: "all-wheel"},
            {name: "Задний", _id: "rear"},
            {name: "Передний", _id: "front"},
        ];
    },
    curCarType:function(){
        var s = Session.get( "carType");
        if(!s){
            Session.set( "carType", {"name":"Все", "_id":0});

        }
        return s.name;
    }
});

Template.t_1.events({
    'click #createSessionBtn': function(e,t){
        e.preventDefault();
        var curRace = Races.find(Router.current().params._id);
        console.log('t', Template.t_1)
        //console.log('create session', this, t_curRace)
        Template.t_1.maxSession++;
        RaceSessions.insert({ name: "Сессия " + Template.t_1.maxSession, race: Router.current().params._id});
        console.log(RaceSessions.find().fetch());
    },
    'click #addRacerBtn': function(e,t){
        e.preventDefault();
        var racerName = t.find('#racerName').value,
            racerSurname = t.find('#racerSurname').value,
            racerNumber = t.find('#racerNumber').value,
            racerCar = t.find('#racerCar').value,
            racerCarType = t.find('#racerCarType').value;

        //var curRace = Races.find(Router.current().params._id);
        //console.log('arr new racer', curRace.fetch()[0])
        Racers.insert({ racerName: racerName, racerSurname: racerSurname,
            racerNumber: racerNumber, racerCar: racerCar, racerCarType: racerCarType});
        $('#addRacerModal').modal('hide');
    },
    'click .removeBtn': function(e,t){
        e.preventDefault();
        Racers.remove(this._id);
    },
    'click #clearBtn': function(e,t){
        Meteor.call('raceLogicClearRacers');
    },
    'click #clearAllTimesBtn': function(e,t){
        Meteor.call('raceLogicClearAllTimes');
    },
    'click #generateDetectLine1': function(e,t){
        var time = new Date();
        Meteor.call('raceLogicIncoming', "1", "L", time.getTime());

    },
    'click #generateDetectLine2': function(e,t){
        var time = new Date();
        Meteor.call('raceLogicIncoming', "2", "L", time.getTime());

    },
    'click .sessionChangeBtn': function(e,t){
        e.preventDefault();
        Session.set( "raceSession", {"name":this.name, "_id":this._id});
    },
    'click #sessionSetBtn': function(e,t){
        e.preventDefault();
        Meteor.call('raceLogicSetSession', Session.get("raceSession")._id);
    },
    'click .startBtn': function(e,t){
        e.preventDefault();


        var racerId = this._id;
        var curRace = Races.find({active: true}).fetch()[0];
        Meteor.call('raceLogicAddRacer', racerId);
        if(!RaceLogicRefreshInterval){
            TimeSync.resync();
            timeOffset = TimeSync.serverOffset();

            RaceLogicRefreshInterval = setInterval(function(){

                Meteor.call('raceLogicRacersCount', function(err, data){
                    if(data === 0){
                        clearInterval(RaceLogicRefreshInterval);
                        RaceLogicRefreshInterval = null;
                    }
                    else{
                        window.A.set(moment().valueOf());
                    }
                });


            }, 55);
        }
        //console.log('racersInRace', Meteor.call('raceLogicRacersCount'))
    },
    'click .changeCarType': function(e,t){
        e.preventDefault();
        Session.set( "carType", {"name":this.name, "_id":this._id});
    },
    'click #exportXML': function(e,t){
        e.preventDefault();
        Meteor.call('raceLogicExportExcel',function (error, result) {
          if(error) {
            console.log(error.reason);
          } else {
            window.location = result;
          }
        });
    }
});
