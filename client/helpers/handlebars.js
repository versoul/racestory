Handlebars.registerHelper('isAdminUser', function() {
    return Roles.userIsInRole(Meteor.user(), ['admin']);
});

Handlebars.registerHelper('equals', function(a, b) {
    return (a == b);
});

var DateFormats = {
    short: "DD MMMM - YYYY",
    long: "dd DD.MM.YYYY HH:mm"
};

UI.registerHelper("formatDate", function(datetime, format) {
    if (moment) {
        // can use other formats like 'lll' too
        //moment.locale('ru');
        format = DateFormats[format] || format;
        return moment(datetime).locale('ru').format(format);
    }
    else {
        return datetime;
    }
});