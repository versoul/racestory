Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'home',
        title:'Домашняя'
    });
    this.route('export', {
        where: 'server',
        path: '/export/:file',
        action: function() {
        var fn = this.params.file.replace(/\.pdf$/, '-' + this.params.path + '.pdf');
        var data;
        try {
            var fs = Npm.require('fs');
            data = fs.readFileSync(process.env.PWD+'/export/' + fn);
        } catch(err) {
            this.response.writeHead(404);
            this.response.end('Error 404 - Not found.');
            return;
        }
        var headers = {
            'Content-type': 'application/octet-stream',
            'Content-Disposition': 'attachment; filename=' + this.params.file
        };
        this.response.writeHead(200, headers);
        this.response.end(data);
    }});
    this.route('accountsAdmin', {
        path:'/accountsAdmin',
        title:'Панель администратора',
        template: 'accountsAdmin',
        onBeforeAction: function() {
            if (Meteor.loggingIn()) {
                console.log("lol")
                this.render(this.loadingTemplate);
            } else if(!Roles.userIsInRole(Meteor.user(), ['admin'])) {
                console.log('redirecting');
                this.redirect('/');
            }else {
                this.next();
            }
        }
    });

    this.route('race/:_id', {
        title:'Заезд',
        waitOn: function() {
            return subsRacers;
        },
        data:function(){

            //return curRace;
        },
        action:function(){
            var curRace = Races.find(this.params._id).fetch()[0];
            var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
            this.render(curRaceConf.template);
        }
    });
    this.route('race_create', {
        path:'/race_create',
        title:'Создание нового заезда',
        template: 'raceCreate'
    });
    this.route('race_list', {
        path:'/race_list',
        title:'Список заездов',
        template: 'raceList'
    });
    this.route('settings', {
        path:'/settings',
        title:'Настройки',
        template: 'settings'
    });
});
