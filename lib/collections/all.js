Races = new Mongo.Collection('races');
Races.allow({
    'insert': function (userId,doc) {
        /* user and doc checks ,
         return true to allow insert */
        console.log('insert to Races ', userId, doc)
        return true;
    },
    'remove': function (userId,doc) {
        /* user and doc checks ,
         return true to allow insert */
        console.log('remove from Races ', userId, doc)
        return true;
    },
    'update': function (userId,doc) {
        /* user and doc checks ,
         return true to allow insert */
        console.log('update Races ', userId, doc)
        return true;
    }
});

Console = new Mongo.Collection('console');


Racers = new Mongo.Collection('racers');
Racers.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});
RaceSessions = new Mongo.Collection('raceSessions');
RaceSessions.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});
Times = new Mongo.Collection('times');
Times.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});

Config = new Mongo.Collection('config');
Config.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});


if(Meteor.isClient){
    Deps.autorun(function () {
        Meteor.subscribe("userStatus");
    });
    Template.header.usersCount = function(){
        return Meteor.users.find({ "status.online": true }).count();
    };
}
if(Meteor.isServer) {
    Meteor.startup(function () {
        Meteor.publish("userStatus", function(){
            return Meteor.users.find({ "status.online": true });
        });
    });
}
