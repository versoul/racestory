
var conf = new function(){
    _this = this;
    this.id = 1;
    this.name = "time atac 2 датчика";
    this.template = "t_1";
    this.racersInRace = [];

    var curRacerNum = 0;

    this.main = function(num, cmd, time){
        console.log("main", num, cmd, time);
        var curRace = Races.find().fetch()[0];//FIXME
        var curRacer = _this.getCurRacer();
        var conf = Config.findOne();
        if(!curRacer){
            return null;
        }
        //console.log("s", curSession)

        Times.insert({ racerId: curRacer.id, sessionId:conf.raceSession, raceId: curRace._id,
            time: time, tmpTime: moment().valueOf(), detectorNum: num});
        curRacerNum++;
        var times = Times.find({ racerId: curRacer.id, sessionId:conf.raceSession, raceId: curRace._id});
        if(!(times.count() % 2)){
            _this.racersInRace.splice(0,1);
            console.log('finish')
            curRacerNum--;
        }
        console.log("T", Times.find().fetch())
    };
    this.addRacerToRace = function(racerId){
        _this.racersInRace.push({"id":racerId});
        console.log('add rcr', _this.racersInRace)
    };
    this.clearRacers = function(){
        _this.racersInRace = [];
        console.log('clear', _this.racersInRace);
    };
    this.racersInRaceCount = function(){
        return _this.racersInRace.length;
    };
    this.clearAllTimes = function(){
        var curRace = Races.find().fetch()[0];//FIXME
        var conf = Config.findOne();
        var times = Times.find({raceId: curRace._id,
             sessionId: conf.raceSession}).fetch();
        for(var i=0,l=times.length; i<l; i++){
            Times.remove(times[i]._id);
        }
        console.log(Times.find({raceId: curRace._id,
             sessionId: conf.raceSession}).fetch());
    };
    this.getCurRacer = function(){
        if(_this.racersInRace.length>0){
            if(curRacerNum != 0 && curRacerNum+1 > _this.racersInRace.length){
                curRacerNum = 0;
            }
            return _this.racersInRace[curRacerNum];
        }
        else{
            return null;
        }

    },
    this.exportExcel = function(){
        var xlsx = Meteor.npmRequire('excel4node')

        var carTypes = [
            {name: "Все", _id: "all"},
            {name: "Полный", _id: "all-wheel"},
            {name: "Задний", _id: "rear"},
            {name: "Передний", _id: "front"},
        ];
        var racers = [];
        var max_row = 1;
        var max_cell = 0;
        var curRace = Races.find().fetch()[0];//FIXME
        var sessions = RaceSessions.find({race: curRace._id}).fetch();

        var wb = new xlsx.WorkBook();
        var ws = null;
        var times = [];

        for(var s=0,sl=sessions.length; s<sl; s++){
            ws = wb.WorkSheet(sessions[s].name);
            for(var ct=0,ctl=carTypes.length; ct<ctl; ct++){
                if(carTypes[ct]._id !== "all"){
                    ws.Cell(max_row,1, max_row, 12, true).String(carTypes[ct].name)
                        .Format.Font.Alignment.Horizontal('center')
                        .Format.Font.Bold();
                    max_row++;

                    racers = Racers.find({racerCarType: carTypes[ct]._id}).fetch();
                    for(var r=0,rl=racers.length; r<rl; r++){
                        ws.Cell(max_row,1).String(racers[r].racerNumber)
                        .Format.Font.Alignment.Horizontal('center');
                        ws.Cell(max_row,2).String(racers[r].racerSurname+' '+racers[r].racerName);
                        ws.Cell(max_row,3).String(racers[r].racerCar);

                        max_cell = 4;
                        times = Times.find({racerId: racers[r]._id, raceId: curRace._id,
                             sessionId: sessions[s]._id}).fetch();
                        for(var t=1,tl=8; t<tl; t++){
                            if(times[2 * t - 1] && times[2 * t - 2]){
                                ws.Cell(max_row, max_cell).String(moment(times[2 * t - 1].time -
                                     times[2 * t - 2].time).format('mm:ss,SSS'))
                                     .Format.Font.Alignment.Horizontal('center');
                            }
                            else{
                                ws.Cell(max_row, max_cell).String('--//--')
                                .Format.Font.Alignment.Horizontal('center');
                            }
                            max_cell++;
                        }
                        max_row++;
                    }
                }
            }
            ws.Column(1).Width(4);
            ws.Column(4).Width(9);
            ws.Column(5).Width(9);
            ws.Column(6).Width(9);
            ws.Column(7).Width(9);
            ws.Column(8).Width(9);
            ws.Column(9).Width(9);
            ws.Column(10).Width(9);
            max_row = 1;
        }
        var base = process.env.PWD;
        wb.write(base+"/export/export.xlsx");
        return '/export/export.xlsx';
    }
};

raceConfs.push(conf);
