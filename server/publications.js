Meteor.publish('races', function() {
    return Races.find();
});
Meteor.publish('racers', function() {
    return Racers.find();
});
Meteor.publish('console', function() {
    return Console.find();
});
Meteor.publish('config', function() {
    return Config.find();
});
Meteor.publish('raceSessions', function() {
    return RaceSessions.find();
});
Meteor.publish('times', function() {
    return Times.find();
});
/*var usage = Meteor.npmRequire('usage');
var pid = process.pid // you can use any valid PID instead
usage.lookup(pid, function(err, result) {
    console.log('stat', result)
});*/
