
/*Meteor.startup(function () {
  //SerialPort = Meteor.npmRequire('serialport');
  //SP = new SerialPort;
  listSerialPorts = function(callback) {
    SerialPort.list(function (err, ports) {
      //console.log('ports', ports);
    });
  }
  //listSerialPorts();
  //console.log('lol', SerialPort)
});*/
var SerialPort = Meteor.npmRequire('serialport');
serialPort = null;
Config.update({}, {$set: {'serialPortStatus': 0}});
listSerialPorts = function(callback) {
  SerialPort.list(function (err, ports) {
    callback(null, ports);
  });
};


Meteor.methods({
    serialPortsRefresh: function () {
        var ports = Meteor.wrapAsync(listSerialPorts);
        var result = ports();
        result = _.map(result, function(key){
            return {"comName": key.comName};
        });
        Config.update({}, {$set: {'serialPortsList': result}});
    },
    serialPortSelect: function(serialPortName){
        Config.update({}, {$set: {'selectedSerialPort': serialPortName}});
        //Config.update({}, {$set: {'serialPortStatus': 0}});
    },
    serialPortConnect: function(){
        var conf = Config.findOne();
        console.log('connect', conf.selectedSerialPort);
        serialPort = new SerialPort.SerialPort(conf.selectedSerialPort, {
            baudrate: 115200,
            parser: SerialPort.parsers.readline('\r\n')
        });


        serialPort.on('open', Meteor.bindEnvironment(function() {
            console.log('Port open');
            Config.update({}, {$set: {'serialPortStatus': 1}});
        }));
        serialPort.on('close', Meteor.bindEnvironment(function() {
            console.log('Port close');
            Config.update({}, {$set: {'serialPortStatus': 0}});
        }));
        serialPort.on('error', Meteor.bindEnvironment(function(err) {
            console.log('Port error',err);
            Config.update({}, {$set: {'serialPortStatus': 0}});
        }));


        serialPort.on('data', Meteor.bindEnvironment(function(data) {
          console.log('incoming', data);
          Console.insert({ data: data});
          var re = new RegExp("-=(.+)=(.+)=(.+)=-");
          var result = data.match(re);
          if(result){
              var num = Math.abs(result[1])+'';
              var cmd = result[2];
              var data = result[3];
              console.log('incoming num = '+num+' cmd = '+cmd+' data = '+data );

              RaceLogic.incomingCallback(num, cmd, parseInt(data));
          }


        }));

    }
});
