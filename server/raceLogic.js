RaceLogic = new function(){

    var _this = this;
    var savedVars = [];
    var curRacerNum = 0;
    var curRacerNumStart = 0;
    var curRacerNumFinish = 0;
    var lapsNum = 3;

    this.racersInRace = [];

    this.clearVars = function(){
        curRacerNumStart = 0;
        curRacerNumFinish = 0;
    }

    this.interpritate = function(cmd, data){
        var curRacer = _this.getCurRacer();
        if(!curRacer){
            return null;
        }

        Racers.update(curRacer._id, {$push: {'racerTimes': data, 'racerTmpTimes':moment().valueOf()}});
        curRacer = _this.getCurRacer();
        curRacerNum++;

        if(!(curRacer.racerTimes.length % lapsNum)){
            _this.racersInRace.splice(0,1);

            //Set best time
            if(curRacer.racerTimes.length == 2){
                var time1 = curRacer.racerTmpTimes[1] - curRacer.racerTmpTimes[0];
                Racers.update(curRacer._id, {$set: {'bestTime': time1}});
            }
            else if(curRacer.racerTimes.length == 4){
                var time1 = curRacer.racerTmpTimes[1] - curRacer.racerTmpTimes[0];
                var time2 = curRacer.racerTmpTimes[3] - curRacer.racerTmpTimes[2];
                Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2)}});
            }
            console.log('finish')
        }
    }

    this.incomingCallback = function(num, cmd, data){


        var curRace = Races.find({ "active": true }).fetch()[0]; //TODO Выбирать только 1

        if(curRace.raceStart === curRace.raceFinish && num === curRace.raceStart){

            var curRacer = _this.getCurRacer("line");
            //console.log("LINE", curRacer)
            if(!curRacer){
                return null;
            }
            Racers.update(curRacer._id, {$push: {'racerTimes': data, 'racerTmpTimes':moment().valueOf()}});
            curRacer = _this.getCurRacer("line");
            curRacerNum++;

            if(!(curRacer.racerTimes.length % curRace.raceLaps)){
                _this.racersInRace.splice(0,1);

                //Set best time
                if(curRacer.racerTimes.length == 2){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    Racers.update(curRacer._id, {$set: {'bestTime': time1}});
                }
                else if(curRacer.racerTimes.length == 4){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2)}});
                }
                else if(curRacer.racerTimes.length == 6){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3)}});
                }
                else if(curRacer.racerTimes.length == 8){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    var time4 = curRacer.racerTimes[7] - curRacer.racerTimes[6];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3, time4)}});
                }
                else if(curRacer.racerTimes.length == 10){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    var time4 = curRacer.racerTimes[7] - curRacer.racerTimes[6];
                    var time5 = curRacer.racerTimes[9] - curRacer.racerTimes[8];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3, time4, time5)}});
                }
                console.log('finish', curRacer._id)
                curRacerNum--;
            }
        }
        else{
            if(num === curRace.raceStart){

                var curRacer = _this.getCurRacer("start");
                //console.log('STRART', curRacer)
                if(!curRacer){
                    return null;
                }
                curRacerNumStart++;
                Racers.update(curRacer._id, {$push: {'racerTimes': data, 'racerTmpTimes':moment().valueOf()}});
            }
            else if(num === curRace.raceFinish){

                var curRacer = _this.getCurRacer("finish");
                //console.log('FINISH', curRacer)
                if(!curRacer){
                    return null;
                }
                //curRacerNumFinish++;
                Racers.update(curRacer._id, {$push: {'racerTimes': data, 'racerTmpTimes':moment().valueOf()}});
                curRacer = _this.getCurRacer("finish");


                console.log(curRacer.racerTimes)

                _this.racersInRace.splice(0,1);
                curRacerNumStart--;
                if(_this.racersInRace.length === 0){
                    _this.clearVars()
                }

                console.log("ololo finish")

                //Set best time
                if(curRacer.racerTimes.length == 2){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    Racers.update(curRacer._id, {$set: {'bestTime': time1}});
                }
                else if(curRacer.racerTimes.length == 4){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2)}});
                }
                else if(curRacer.racerTimes.length == 6){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3)}});
                }
                else if(curRacer.racerTimes.length == 8){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    var time4 = curRacer.racerTimes[7] - curRacer.racerTimes[6];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3, time4)}});
                }
                else if(curRacer.racerTimes.length == 10){
                    var time1 = curRacer.racerTimes[1] - curRacer.racerTimes[0];
                    var time2 = curRacer.racerTimes[3] - curRacer.racerTimes[2];
                    var time3 = curRacer.racerTimes[5] - curRacer.racerTimes[4];
                    var time4 = curRacer.racerTimes[7] - curRacer.racerTimes[6];
                    var time5 = curRacer.racerTimes[9] - curRacer.racerTimes[8];
                    Racers.update(curRacer._id, {$set: {'bestTime': Math.min(time1, time2, time3, time4, time5)}});
                }
            }

            if(!curRacer){
                return null;
            }


        }

    }

    this.clearAllTimes = function(){
        console.log('cear all times')
        var racers = Racers.find().fetch();
        _.each(racers, function(obj){
            Racers.update(obj._id, {$set: {'racerTimes': [], 'racerTmpTimes':[], 'bestTime':9999999999}});
        });
    }

    this.getCurRacer = function(val){
        if(val === "line"){
            //console.log("GET CUR RACER LINE", curRacerNum, _this.racersInRace.length)
            if(curRacerNum != 0 && curRacerNum+1 > _this.racersInRace.length){
                curRacerNum = 0;
            }
            return Racers.find(_this.racersInRace[curRacerNum]).fetch()[0];
        }
        else if(val === "start"){
            //console.log("GET CUR RACER START", curRacerNumStart, _this.racersInRace.length)
            if(curRacerNumStart != 0 && curRacerNumStart+1 > _this.racersInRace.length){
                return null;
            }
            return Racers.find(_this.racersInRace[curRacerNumStart]).fetch()[0];
        }
        else if(val === "finish"){
            //console.log("GET CUR RACER FINISH", curRacerNumFinish, _this.racersInRace.length)
            if(curRacerNumFinish != 0 && curRacerNumFinish+1 > _this.racersInRace.length){
                curRacerNumFinish = 0;
            }
            return Racers.find(_this.racersInRace[curRacerNumFinish]).fetch()[0];
        }

    }
}



Meteor.methods({
    raceLogicClearAllTimes: function () {
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        return curRaceConf.clearAllTimes();
    },
    raceLogicClearRacers: function(){
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        curRaceConf.clearRacers()
        //RaceLogic.clearVars();
        //RaceLogic.racersInRace = [];
    },
    raceLogicIncoming: function(num, cmd, time){
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        curRaceConf.main(num, cmd, time)
        //RaceLogic.incomingCallback(num, cmd, time);
    },
    raceLogicAddRacer: function(racerId, sessionId){
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        curRaceConf.addRacerToRace(racerId, sessionId);

    },
    raceLogicRacersCount: function(){
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        return curRaceConf.racersInRaceCount();
    },
    raceLogicExportExcel: function(){
        var curRace = Races.find().fetch()[0];//FIXME
        var curRaceConf = _.findWhere(raceConfs, {id:curRace.conf});
        return curRaceConf.exportExcel();
    },
    raceLogicSetSession: function(sessionId){
        Config.update({}, {$set: {'raceSession': sessionId}});
    }
});
